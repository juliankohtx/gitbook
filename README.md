# Welcome to the Tezos Wiki!

This is a one-stop-shop to learn everything about Tezos and to answer the frequently asked questions about the protocol & the Tezos ecosystem.


This wiki will be a living document that will evolve as the Tezos protocol evolves. If interested in proposing a change to this document, feel free to make a pull request to [this Gitlab repository](https://gitlab.com/juliankohtx/gitbook).

---
# Getting started
This wiki  covers a variety of topics ranging from Baking to Proof-of-Stake and resources like block explorers for Tezos. 

The best way to get started learning about Tezos is with this video:
<iframe width="640" height="360" src="https://www.youtube.com/embed/ftA7O04yxXg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


----

[ci]: https://about.gitlab.com/gitlab-ci/
[GitBook]: https://www.gitbook.com/
[host the book]: https://gitlab.com/pages/gitbook/tree/pages
[install]: http://toolchain.gitbook.com/setup.html
[documentation]: http://toolchain.gitbook.com
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
