# Summary

* [Introduction to the Tezos Wiki](README.md)
* [Tezos Whitepaper](files/whitepaper.md)

### Tezos Basics

* [What is Tezos?](files/basics.md#intro)
* [What is XTZ?](files/basics.md#xtz)
* [Why is Tezos unique or interesting?](files/basics.md#unique)
* [What shortcomings of other blockchains is Tezos solving?](files/basics.md#shortcomings)
* [What use cases are uniquely suited for Tezos?](files/basics.md#use-case)

### Proof-of-Stake
* [What is Proof-of-Stake?](files/proofofstake.md)
* [What consensus algorithm does Tezos use?](files/proofofstake.md#consensus)
* [Nothing-at-stake problem](files/proofofstake.md#nothing-at-stake)
* [Do transactions have finality?](files/proofofstake.md#finality)
* [How scalable is Tezos?](files/proofofstake.md#scalability)
* [What is the roadmap for improving scalability?](files/proofofstake.md#roadmap)

### Baking
* [What is baking?](files/baking.md#what)
* [How much can I earn by baking or delegating?](files/baking.md#earn)
* [Should I bake or delegate?](files/baking.md#bakeordelegate)
* [What is the difference between implicit and originated accounts?](files/baking.md#implicit)
* [How should I select a baker to delegate with?](files/baking.md#bakerselection)
* [Baking Resources](files/baking.md#resources)

### Self Amendment
* [What is Self Amendment?](files/self-amendment.md#introduction)
* [How does it work?](files/self-amendment.md#how)
* [Arguments for and against on-chain governance](files/self-amendment.md#arguments)


### Michelson & Liquidity
* [Language FAQ](files/language.md#faq)
* [Michelson Language](files/language.md#michelson)
* [Liquidity Language](files/language.md#liquidity)
* [Language Resources](files/language.md#resources)

### Formal Verification
* [Introduction](files/formal-verification.md#intro)
* [Michelson and GADT](files/formal-verification.md#gadt)
* [Michelson and Coq](files/formal-verification.md#coq)

### Future Developments
* [Privacy](files/future.md#privacy)
* [Consensus](files/future.md#consensus)
* [Layer 2](files/future.md#layer2)
* [Governance](files/future.md#governance)

### Built on Tezos
* [Projects building on Tezos](https://tezosprojects.com/index.html)

### Resources
* [Tezos Foundation](files/resources.md#foundation)
* [Tezos Grant Programs](files/resources.md#grant)
* [Wallets](files/resources.md#wallet)
* [Block Explorer](files/resources.md#explorer)